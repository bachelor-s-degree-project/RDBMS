var exports = module.exports = {}

exports.useridcheck = function(req, res) {
  var userinfo =req.user;
  var mses = req.query.valid;
  res.render('pages/useridcheck', {
    userinfo:userinfo,
    messages: mses,
  });
}

exports.signin = function(req, res) {
  var mses = req.query.valid;
  res.render('pages/signin', {
    messages: mses,
  });
}

exports.dashboard = function(req, res) {
  var userinfo =req.user;
  res.render('pages/index', {
    userinfo:userinfo,
  });
}

exports.logout = function(req, res) {
  req.logout();
  req.session.destroy(function(err) {
    res.redirect('/');
  });

}